#!/usr/bin/python

import sys
from ZODB.FileStorage import FileStorage	# easy_install ZODB
from ZODB.DB import DB
import transaction

filename = sys.argv[1]

storage = FileStorage('data.fs')
db = DB(storage)
connection = db.open()
root = connection.root()
root["tc"] = []

with open(filename) as fp:
	# Index the test cases
	last = 0
	case = {}
	for line in fp:
		if "UseCase" in line:
			case = {}
			case["step"] = []	# Required to append below
			case["expected"] = []	# Required to append below
		elif "// Purpose" in line:
			case["purpose"] = line.split("=", 1)[1][:-1]
		elif "// Step" in line:
			case["step"].append(line.split(": ", 1)[1][:-1])
		elif "// Expected" in line:
			case["expected"].append(line.split(": ", 1)[1][:-1])
		elif "// TestEnd" in line:
			root["tc"].append(case)

transaction.commit()
