#!/usr/bin/python
#
#	make-html.py - Generate an HTML file
#
# TODO(PM): http://google-styleguide.googlecode.com/svn/trunk/htmlcssguide.xml
# TODO(PM): http://w3c.github.io/tidy-html5

import sys

def create(cases):
	page = [
		'<!DOCTYPE HTML>'
		'<html lang="en">',
		'<head>',
		'<meta charset="utf-8" />'
		'<link rel="stylesheet" type="text/css" href="screen.css" />',
		'<link rel="shortcut icon" href="favicon.ico" />',
		'<script type="text/javascript" src="library.js"></script>',
		'<title>Editor</title>'
		'</head>',
		'<body>',
		'<div id="header">',
		'<div id="buttons">',
		'<button type="button" onclick="requestCreateTestCase(this.parentNode.parentNode)" disabled>Duplicate</button>',
		'<button type="button" onclick="requestDeleteTestCase(this.parentNode.parentNode)">Delete</button>',
		'</div>',
		'<table id="table-header">',
		'<tr>',
		'<th></th>',
		'<th>Purpose</th>',
		'<th>Steps</th>',
		'<th>Expected</th>',
		'</tr>',
		'</table>',
		'</div>',
		'<div id="content">',
		'<table id="table-content">'
	]
	for index, value in enumerate(cases):
		tcid = str(index)
		page.extend([
			'<tr id="' + tcid + '">',
			'<td class="purpose">',
			'<p id="0" class="attribute" contenteditable>' + value['purpose'] + '</p>',
			'</td>',
			'<td class="step">'
		])
		for index, step in enumerate(value['step']):
			page.append('<p id="' + str(index) + '" class="attribute" contenteditable>' + step + '</p>')
		page.extend([
			'</td>',
			'<td class="expected">',
		])
		for index, result in enumerate(value['expected']):
			page.append('<p id="' + str(index) + '" class="attribute" contenteditable>' + result + '</p>')
		page.extend([
			'</td>',
			'</tr>'
		])
	page.extend([
		'</table>',
		'</div>',
		'</body>',
		'</html>'
	])
	return page

def write(page):
	with open('index.html', 'w') as store:
		for line in page:
			store.write(line)

if __name__ == "__main__":
	from ZODB.DB import DB
	from ZODB.FileStorage import FileStorage
	storage = FileStorage('data.fs')
	db = DB(storage)
	connection = db.open()
	root = connection.root()
	page = create(root["tc"])
	write(page)

