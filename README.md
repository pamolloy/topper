The background of a test case attribute turns yellow when it's contents are changed. It is reset to white when the client receives confirmation from the server that the change was stored and the server has attempted to notify all other clients of the change.

If more than one client is viewing a set of test cases and one client makes a change then all other clients will be notified of that change. For example, if a client changes the contents of an attribute then that text field will turn red in all of the other clients. If another client then modifies an attribute with a red background it will overwrite the edits made by original client and the background of the attribute in original client will appear red. This scenario can easily be tested by opening the web page in two or more tabs of a web browser and making different changes to each tab.

Find and replace functionality is accessible using third-party browser extensions.

## Protocol

The protocol consists of the following message formats:

- Add a test case
	- Request: `{"command":"add",":"", "attr":"", "attrid":"","text":""}`
	- Response: `{"command":"add","tcid":"<index>", "attr":"", "attrid":"","text":""}`
- Add an attribute
	- Request: `{"command":"add","tcid":"<index>", "attr":"<type>", "attrid":"","text":""}`
	- Response: `{"command":"add","tcid":"<index>", "attr":"<type>", "attrid":<index>,"text":""}`
- Remove a test case
	- Request: `{"command":"remove","tcid":"<index>", "attr":"", "attrid":"","text":""}`
	- Response: `{"command":"remove","tcid":"<index>", "attr":"<type>", "attrid":<index>,"text":""}`
- Remove an attribute
	- Request: `{"command":"remove","tcid":"<index>", "attr":"<type>", "attrid":<index>,"text":""}`
	- Response: `{"command":"remove","tcid":"<index>", "attr":"<type>", "attrid":<index>,"text":""}`
- Update an attribute
	- Request: `{"command":"update","tcid":"<index>", "attr":"<type>", "attrid":<index>,"text":"<text>"}`
	- Response: `{"command":"remove","tcid":"<index>", "attr":"<type>", "attrid":<index>,"text":"<text>"}`

The keys `before` or `after` may be included in a message containing "command":"add"` to specify where the attribute should be added. When neither key is included then the attribute will be added at the end of the list.

Valid attribute types include `purpose`, `step`, and `result`.

Note that the design of these messages does not allow inserting a new attribute at a particular index.
