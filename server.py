#!/usr/bin/python
#
#	server.py
#

# Python standard library
import json
import os.path
import threading
from SimpleHTTPServer import SimpleHTTPRequestHandler
import SocketServer

# External libraries
from ZODB.FileStorage import FileStorage
from ZODB.DB import DB
import transaction
from tornado import websocket, web, ioloop

# Custom modules
import page	# TODO(PM): Should my modules be capitalized?

# See http://www.tornadoweb.org/en/stable/websocket.html
class WebSocketHandler(websocket.WebSocketHandler):
	waiters = set()
	delimiter = '\x1E'

	# TODO(PM): These functions should be outside the server source code
	def create_testcase(self):
		index = len(cases) - 1
		cases.append({"purpose":"","step":[],"expected":[]})
		return index

	def delete_testcase(self, tc):
		cases.pop(tcid)

	def create_attribute(self, tcid, attr):
		cases[tcid][attr].append("")
		index = len(cases[tcid][attr]) - 1
		return index

	def delete_attribute(self, tcid):
		cases[tcid][attr].pop(attrid)

	def update_attribute(self, tcid, attr, attrid, text):
		if attr == "purpose":
			cases[tcid][attr] = text
		else:
			cases[tcid][attr][attrid] = text

	# https://github.com/facebook/tornado/blob/master/demos/websocket/chatdemo.py
	@classmethod
	def broadcast(cls, msg):
		for waiter in cls.waiters:
			waiter.write_message(json.dumps(msg))

	def open(self):
		print "WebSocket opened"
		WebSocketHandler.waiters.add(self)
		# TODO(PM): Sending out all test cases in case the client was offline

	def on_message(self, message):
		msg = json.loads(message)

		if "tcid" in msg:
			msg["tcid"] = int(msg["tcid"])
		if "attrid" in msg:
			msg["attrid"] = int(msg["attrid"])

		print msg
		if msg["command"] == "update":
			self.update_attribute(msg["tcid"], msg["attr"], msg["attrid"], msg["text"])
			self.broadcast(msg)
		elif msg["command"] == "add":
			if "attr" in msg:
				msg["attrid"] = self.create_attribute(msg["tcid"], msg["attr"])
			else:
				msg["tcid"] = self.create_testcase()
			self.broadcast(msg)
		elif msg["command"] == "remove":
			if "attr" in msg:
				self.delete_attribute(msg["tcid"], msg["attr"], msg["attrid"])
			else:
				self.delete_testcase(msg["tcid"])
			self.broadcast(msg)
		else:
			pass

	def on_close(self):
		print "WebSocket closed"
		WebSocketHandler.waiters.remove(self)

class WebSocketServer(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		app = web.Application([(r"/websocket", WebSocketHandler)])
		app.listen(2222)	# Start listening

	def run(self):
		ioloop.IOLoop.instance().start()

class Handler(SimpleHTTPRequestHandler):
	def do_GET(self):
		p = page.create(cases)
		page.write(p)
		SimpleHTTPRequestHandler.do_GET(self)

class HTTPServer(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.httpd = SocketServer.TCPServer(("", 8000), Handler)

	def run(self):
		self.httpd.serve_forever()

if __name__ == "__main__":
	# Initialize and start database
	storage = FileStorage('data.fs')
	db = DB(storage)
	connection = db.open()
	root = connection.root()
	cases = root["tc"]
	# Initialize and start servers
	httpserver = HTTPServer()
	httpserver.start()
	wsserver = WebSocketServer()
	wsserver.start()
