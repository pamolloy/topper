document.addEventListener("input", function(e){requestUpdateAttribute(e.target)}, false);	//TODO(PM): I hate Javascript
document.addEventListener("keydown", function(e){newLineHandler(e)}, false);
document.addEventListener("click", function(e){console.log(e.target)}, false);

var delimiter = "\x1E";
// See https://kuler.adobe.com
var errorColor = "#FF6863";
var warningColor = "#FFE263";
var goodColor = "#B1C8E8";

// See https://developer.mozilla.org/en-US/docs/WebSockets
if ("WebSocket" in window) {
	var ws = new WebSocket("ws://localhost:2222/websocket");
	ws.onopen = function() {
		console.log("Socket open");
	};
	ws.onmessage = function(e) {
		var msg = JSON.parse(e.data);
		console.log(msg);
		if (msg.command == "update") {
			updateAttribute(msg.tcid, msg.attr, msg.attrid, msg.text);
		} else if (msg.command == "add") {
			if (msg.attr) {
				createAttribute(msg.tcid, msg.attr, msg.attrid);
			} else {
				createTestCase(msg.tcid);
			}
		} else if (msg.command == "remove") {
			if (msg.attr) {
				deleteAttribute(msg.tcid, msg.attr, msg.attrid);
			} else {
				deleteTestCase(msg.tcid);
			}
		} else {
			console.log("Invalid command in message: " + msg.command)
		}
	};
	ws.onclose = function() {
		console.log("Socket closed");
	};
	ws.onerror = function () {
		console.log("Socket error");
	};
} else {
  console.log("This browser doesn't support WebSockets");
}

function deleteTestCase(tcid) {
	var tc = getRowByIndex(tcid);
	tc.parentNode.removeChild(tc);			// Remove the row
}

// TODO(PM): Add blank attributes to test case
function createTestCase(tcid) {
	document.getElementById("table-content").insertRow();
}

function deleteAttribute(tcid, attr, attrid) {
	var cell = getCellByClassCell(tcid, attr);
	cell.removeChild(cell.childNodes[attrid]);
}

function createAttribute(tcid, attr, attrid) {
	var cell = getCellByClass(tcid, attr);
	var p = document.createElement("p");
	p.contentEditable = "true";
	cell.appendChild(p);	
}

function updateAttribute(tcid, attr, attrid, text) {
	var cell = getCellByClass(tcid, attr);
	var p = cell.childNodes[attrid];
	if (p.innerHTML !== text) {
		p.style.background=errorColor;
	} else {
		p.style.background="inherit";
	}
}

// Return a test case using the test case ID
function getRowByIndex(tcid) {
	var table = document.getElementById("table-content");
	var tc = table.rows[tcid];
	return tc;
}

// Return a test case attribute using the test case ID and attribute type
function getCellByClass(tcid, attr) {
	var tc = getRowByIndex(tcid);
	for (var i=0; i<tc.childNodes.length; i++) {
		var cell = tc.childNodes[i];
		if (cell.className == attr) {
			return cell;
		}
	}
}

// Duplicate the element below
//function duplicateRowBelow(element) {
//	var row = element.cloneNode(true);
//	row.id = "";	//TODO(PM): Dynamically assign value, note static code in HTML is broken
//	// If next row exists, then insert before next row, otherwise append to table
//	if (element.nextSibling) {
//		element.parentNode.insertBefore(row, element.nextSibling);
//	} else {
//		element.parentNode.appendChild(row);
//	}
//}

// SEND
function requestCreateAttribute(button) {
	var cell = button.parentNode;
	var row = cell.parentNode;
	notify({"command":"add", "tcid":row.id, "attr":cell.className});
}

function requestDeleteAttribute(attr) {
	attr.style.background=warningColor;
	var cell = attr.parentNode;
	var row = cell.parentNode;
	notify({"command":"remove", "tcid":row.id, "attr":cell.className, "attrid":attr.id});
}

function requestUpdateAttribute(attr) {
	attr.style.background=warningColor;
	var cell = attr.parentNode;
	var row = cell.parentNode;
	var attrid = Array.prototype.indexOf.call(cell.childNodes, attr);
	notify({"command":"update", "tcid":row.id, "attr":cell.className, "attrid":attrid, "text":attr.innerHTML});
}

// TODO(PM): The protocol needs to be better architectured to handle this case
function requestCreateTestCase(row) {
	notify({"command":"add"});
}

function requestDeleteTestCase() {
	row.style.background=warningColor;
	var tcid = 
	notify({"command":"remove", "tcid":tcid});
}

// http://stackoverflow.com/questions/888256
function notify(msg) {
	if (ws.readyState == 1) {
		ws.send(JSON.stringify(msg));
	}
}

function newLineHandler(e) {
	if (e.keyCode == 13) {
		e.preventDefault();
		requestCreateAttribute(e.target);
		// TODO(PM): Cannot append to Purpose!
	}
}

